import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './TodoItem.css';

export default class TodoItem extends Component {
  getStyle = () => {
      return this.props.todo.completed ? 'todo completed' : 'todo';
  };
 
  render() {
    const {id, title} = this.props.todo;
    return ( 
        <div className={this.getStyle()}> 
            <input type="checkbox" onChange={this.props.toggleCheck.bind(this, id)}/> {''}
            {title}
            <button onClick={this.props.removeTodo.bind(this, id)} className="btn">X</button>
        </div>
    )
  }
}

//PropTypes
TodoItem.propTypes = {
    todo: PropTypes.object.isRequired,
    toggleCheck: PropTypes.func.isRequired,
    removeTodo: PropTypes.func.isRequired
}

  