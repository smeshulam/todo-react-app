import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TodoItem from './TodoItem';

export default class Todos extends Component {
  render() {
    return ( 
        <div> 
            {
              this.props.todos.map(todo => (
                  <li key={todo.id}>
                    <TodoItem todo={todo} toggleCheck={this.props.toggleCheck} removeTodo={this.props.removeTodo}/>
                  </li>
              ))
            }
        </div>
    )
  }
}

//PropTypes
Todos.propTypes = {
  todos: PropTypes.array.isRequired,
  toggleCheck: PropTypes.func.isRequired,
  removeTodo: PropTypes.func.isRequired
}
