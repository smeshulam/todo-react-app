import React, {Component} from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import axios from 'axios';

import Header from './components/layout/Header';
import About from './components/pages/About';
import Todos from './components/Todos';
import AddTodo from './components/AddTodo';

export default class App extends Component {
  state = {
    todos: []
  };

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/todos?_limit=10')
      .then(res => this.setState({todos: res.data}))
  }

  //Add todo
  addTodo = (title) => {
    /*const newTodo = {
      id: uuid.v4(),
      title,
      completed: false
    }
    this.setState({todos: [...this.state.todos, newTodo]});*/
    axios.post('https://jsonplaceholder.typicode.com/todos', {
      title,
      completed: false
    })
    .then(res => this.setState({todos: [...this.state.todos, res.data]}))
    
  }

  //Toggle complete
  toggleCheck = (id) => {
     this.setState({
        todos:this.state.todos.map(todo => {
          if(todo.id === id) {
            todo.completed = !todo.completed;
          }
          return todo;
        })     
    })
  };  

  removeTodo = (id) => {
    axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
    .then(res => this.setState({
      todos: [...this.state.todos.filter(todo => todo.id !== id)] 
    }))
    /*this.setState({
      todos: [...this.state.todos.filter(todo => todo.id !== id)] 
    })*/
  }

  render() {
    return ( 
      <BrowserRouter>
        <div className="App">
          <div className=".container">
            <Header/>
            <Route exact path="/" render={props =>(
              <React.Fragment>
                <ul> 
                  <Todos todos={this.state.todos} toggleCheck={this.toggleCheck} removeTodo={this.removeTodo}/>
                </ul>
                <AddTodo addTodo={this.addTodo}/>
              </React.Fragment>
            )} />
            <Route path="/about" component={About} />
          </div>
        </div>
      </BrowserRouter>
    )
  }
}
